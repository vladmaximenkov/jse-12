package ru.vmaksimenkov.tm.repository;

import ru.vmaksimenkov.tm.api.repository.ICommandRepository;
import ru.vmaksimenkov.tm.constant.ArgumentConst;
import ru.vmaksimenkov.tm.constant.TerminalConst;
import ru.vmaksimenkov.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
        TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Show developer info"
    );

    private static final Command HELP = new Command(
        TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Show terminal commands"
    );

    private static final Command VERSION = new Command(
        TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Show program version"
    );

    private static final Command EXIT = new Command(
        TerminalConst.CMD_EXIT, null, "Close program"
    );

    private static final Command INFO = new Command(
        TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Show system info"
    );

    private static final Command COMMANDS = new Command(
        TerminalConst.CMD_COMMANDS, null, "Show program commands"
    );

    private static final Command ARGUMENTS = new Command(
        TerminalConst.CMD_ARGUMENTS, null, "Show program arguments"
    );

    private static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null, "Create new task"
    );

    private static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null, "Clear all tasks"
    );

    private static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null, "Show task list"
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null, "Update task by index"
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null, "Update task by id"
    );

    private static final Command TASK_UPDATE_BY_NAME = new Command(
            TerminalConst.TASK_UPDATE_BY_NAME, null, "Update task by name"
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.TASK_VIEW_BY_ID, null, "View task by id"
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.TASK_VIEW_BY_INDEX, null, "View task by index"
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.TASK_VIEW_BY_NAME, null, "View task by name"
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null, "Remove task by id"
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null, "Remove task by index"
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.TASK_REMOVE_BY_NAME, null, "Remove task by name"
    );

    private static final Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null, "Start task by id"
    );

    private static final Command TASK_START_BY_NAME = new Command(
            TerminalConst.TASK_START_BY_NAME, null, "Start task by name"
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null, "Start task by index"
    );

    private static final Command TASK_FINISH_BY_ID = new Command(
            TerminalConst.TASK_FINISH_BY_ID, null, "Finish task by id"
    );

    private static final Command TASK_FINISH_BY_NAME = new Command(
            TerminalConst.TASK_FINISH_BY_NAME, null, "Finish task by name"
    );

    private static final Command TASK_FINISH_BY_INDEX = new Command(
            TerminalConst.TASK_FINISH_BY_INDEX, null, "Finish task by index"
    );

    private static final Command TASK_SET_STATUS_BY_ID = new Command(
            TerminalConst.TASK_SET_STATUS_BY_ID, null, "Set status of the task by id"
    );

    private static final Command TASK_SET_STATUS_BY_NAME = new Command(
            TerminalConst.TASK_SET_STATUS_BY_NAME, null, "Set status of the task by name"
    );

    private static final Command TASK_SET_STATUS_BY_INDEX = new Command(
            TerminalConst.TASK_SET_STATUS_BY_INDEX, null, "Set status of the task by index"
    );

    private static final Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null, "Create new project"
    );

    private static final Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null, "Clear all projects"
    );

    private static final Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null, "Show project list"
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null, "Update project by index"
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null, "Update project by id"
    );

    private static final Command PROJECT_UPDATE_BY_NAME = new Command(
            TerminalConst.PROJECT_UPDATE_BY_NAME, null, "Update project by name"
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.PROJECT_VIEW_BY_ID, null, "View project by id"
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.PROJECT_VIEW_BY_INDEX, null, "View project by index"
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.PROJECT_VIEW_BY_NAME, null, "View project by name"
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null, "Remove project by id"
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null, "Remove project by index"
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.PROJECT_REMOVE_BY_NAME, null, "Remove project by name"
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null, "Start project by id"
    );

    private static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.PROJECT_START_BY_NAME, null, "Start project by name"
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null, "Start project by index"
    );

    private static final Command PROJECT_FINISH_BY_ID = new Command(
            TerminalConst.PROJECT_FINISH_BY_ID, null, "Finish project by id"
    );

    private static final Command PROJECT_FINISH_BY_NAME = new Command(
            TerminalConst.PROJECT_FINISH_BY_NAME, null, "Finish project by name"
    );

    private static final Command PROJECT_FINISH_BY_INDEX = new Command(
            TerminalConst.PROJECT_FINISH_BY_INDEX, null, "Finish project by index"
    );

    private static final Command PROJECT_SET_STATUS_BY_ID = new Command(
            TerminalConst.PROJECT_SET_STATUS_BY_ID, null, "Set status of the project by id"
    );

    private static final Command PROJECT_SET_STATUS_BY_NAME = new Command(
            TerminalConst.PROJECT_SET_STATUS_BY_NAME, null, "Set status of the project by name"
    );

    private static final Command PROJECT_SET_STATUS_BY_INDEX = new Command(
            TerminalConst.PROJECT_SET_STATUS_BY_INDEX, null, "Set status of the project by index"
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
        ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS,
        TASK_CREATE, TASK_CLEAR, TASK_LIST,
        TASK_UPDATE_BY_INDEX, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_NAME,
        TASK_VIEW_BY_ID, TASK_VIEW_BY_NAME, TASK_VIEW_BY_INDEX,
        TASK_REMOVE_BY_ID, TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_INDEX,
        TASK_START_BY_ID,TASK_START_BY_NAME,TASK_START_BY_INDEX,
        TASK_FINISH_BY_ID,TASK_FINISH_BY_NAME,TASK_FINISH_BY_INDEX,
        TASK_SET_STATUS_BY_ID,TASK_SET_STATUS_BY_NAME,TASK_SET_STATUS_BY_INDEX,
        PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST,
        PROJECT_UPDATE_BY_INDEX, PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_NAME,
        PROJECT_VIEW_BY_ID, PROJECT_VIEW_BY_NAME, PROJECT_VIEW_BY_INDEX,
        PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_INDEX,
        PROJECT_START_BY_ID,PROJECT_START_BY_NAME,PROJECT_START_BY_INDEX,
        PROJECT_FINISH_BY_ID,PROJECT_FINISH_BY_NAME,PROJECT_FINISH_BY_INDEX,
        PROJECT_SET_STATUS_BY_ID,PROJECT_SET_STATUS_BY_NAME,PROJECT_SET_STATUS_BY_INDEX,
        EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
